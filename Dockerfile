FROM php:apache

ARG VANILLA_VERSION='3.3'
ENV VANILLA_VERSION ${VANILLA_VERSION}

RUN apt-get update && apt-get install -y unzip nodejs git gnupg2 libicu-dev libfreetype6-dev libjpeg62-turbo-dev libpng-dev libonig-dev exiftool sendmail \
    && curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list \
    && apt-get update && apt-get install -y yarn \
    && curl -sSL "https://github.com/vanilla/vanilla/archive/${VANILLA_VERSION}.zip" -o vanilla.zip \
    && unzip vanilla.zip \
    && cp -rT vanilla-${VANILLA_VERSION} /var/www/html \
    && rm -rf vanilla* \
    && chmod -R 777 /var/www/html/conf \
    && chmod -R 777 /var/www/html/cache \
    && curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer \
    && chmod +x /usr/local/bin/composer \
    && docker-php-ext-install -j$(nproc) pdo pdo_mysql intl gd mbstring \
    && rm -rf /var/lib/apt/lists/* /usr/src/php/ext/* /tmp/* \
    && chown -R www-data:www-data /var/www/html/uploads \
    && a2enmod rewrite

RUN sed -i 's/VANILLA_BUILD_DISABLE_AUTO_BUILD=true//' ./bin/validateDeps.sh && \
    ./bin/validateDeps.sh

VOLUME /var/www/html/uploads
